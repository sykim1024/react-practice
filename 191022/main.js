$(document).ready(function(){
	 var time = 5;
    var $bar,
        $slick,
        isPause,
        tick,
        percentTime;

    $slick = $('.main-slider').on('init', function(event, slick) {
		$(event.target).find(".slick-active").addClass("slick-animated");
	}).slick({
      draggable: true,
      autoplay: false,
      //autoplaySpeed: 7000,
      arrows: false,
      fade: true,
      speed: 700,
      infinite: true,
      cssEase: 'ease-in-out',
      touchThreshold: 100,
      dots: true,
	  waitForAnimate: false,
      customPaging : function(slider, i) {
				var parent = $(".main-slider-item:eq("+i+")");
        return "<div class='sd-con sd"+i+"' ><a href='#' onclick='return false'>"+parent.attr("data-title")+"</a></div>";
      }
    }).on('afterChange', function(event, slick, currentSlide, nextSlide){
  		$(event.target).find(".slick-animated").removeClass("slick-animated");
  		$(event.target).find(".slick-active").addClass("slick-animated");
  	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
			$(".slick-dots li:eq("+currentSlide+")").removeClass("slick-animated");
  		$(".slick-dots li:eq("+nextSlide+")").addClass("slick-animated");
  	});


  // $(".slick-dots li").on("click",function(){
  //   $(this).addClass("slick-animated");
  //   $(this).siblings().removeClass("slick-animated");
  // });

    $bar = $('#progress-bar .bar');
    function startProgressbar() {
      resetProgressbar();
      percentTime = 0;
      isPause = false;
      tick = setInterval(interval, 13);
    }
    function stopProgressbar(){
       isPause = true;
    }
    function interval() {
      if(isPause === false) {
        percentTime += 1 / (time+0.1);
        $bar.css({
          width: percentTime+"%"
        });
        if(percentTime >= 100)
          {
            $slick.slick('slickNext');
            startProgressbar();
          }
      }
    }
    function resetProgressbar() {
      $bar.css({
       width: 0+'%'
      });
      clearTimeout(tick);
    }
    startProgressbar();
    $(".slick-dots li").on("click",function(){startProgressbar();})
    $('.main-slider').on('swipe', function(){startProgressbar();});


  $(window).bind('scroll', function () {
    if($(window).scrollTop() > 1) {
      $("#progress-bar").css("opacity","0")
      stopProgressbar();
    }else {
      $("#progress-bar").css("opacity","1");
      startProgressbar();
    }
  });

   $(".cate-slider").slick({
      draggable: true,
      autoplay: true,
      autoplaySpeed: 5000,
      arrows: false,
      speed: 700,
      fade: true,
      infinite: true,
      cssEase: 'ease-in-out',
      swipe: true,
      swipeToSlide: true,
      touchThreshold: 100,
      dots: true,
       customPaging : function(slider, i) {
        return "<a href='#'' class='basic-dot basic-dot' onclick='return false'></a>";
      }
    });

    $(document).on("mouseover",".show .cate-left .btn-bottom a",function(){
      var eleNum = $(this).index();
      $(".show .cate-slider .slick-dots").find("li").eq(eleNum).trigger("click");
      $('.cate-slider').slick('slickPause')
    });
    $(document).on("mouseout",".show .cate-left .btn-bottom a",function(){
      $('.cate-slider').slick('slickPlay')
    });




    /*magazine*/
	  var $magazine = $('.magazine-slider');
	  var slideCount = null;
	  $magazine.on('init', function(event, slick){
	    slideCount = slick.slideCount;
	    setSlideCount();
	    setCurrentSlideNumber(slick.currentSlide);
	  }).slick({
	      draggable: true,
	      autoplay: true,
	      autoplaySpeed: 5000,
	      arrows: false,
	      speed: 700,
	      fade: true,
	      infinite: true,
	      cssEase: 'ease-in-out',
	      swipe: true,
	      swipeToSlide: true,
	      touchThreshold: 100,
	      dots: false
	    });

	  $magazine.on('beforeChange', function(event, slick, currentSlide, nextSlide){
	    setCurrentSlideNumber(nextSlide);
	  });
	  function setSlideCount() {
	    var $el = $('.slide-count-wrap').find('.total');
	    $el.text(slideCount);

	  }
	  function setCurrentSlideNumber(currentSlide) {
	    var $el = $('.slide-count-wrap').find('.current');
	    $el.text(currentSlide + 1);
	  }
	  $('.mz-prev').click(function(){$('.magazine-slider').slick('slickPrev');})
	  $('.mz-next').click(function(){$('.magazine-slider').slick('slickNext');})


	  $(".magazine-list > li").on({
	    mouseover:function(){
	      $(this).children(".mz-box").stop().animate({top: '-30px'});
	    },
	    mouseout:function(){
	      $(this).children(".mz-box").stop().animate({top: '0px'});
	    }
	  })



	  $(window).scroll(function() {
	    var scroll = $(window).scrollTop();
		  var scroll_middle = scroll + $(window).innerHeight() / 2;
		  var boundary = $(".award-area").offset().top;
		  if(remove_enable) {
			  if($grid.find('.grid-item').length <= 3)
	  		{
	  			remove_enable = false;
	  			$('.social-con .btn-more a').stop().animate({opacity:"1"});
	  			count = $grid.find('.grid-item').length;
	  			//console.log("folded");
	  		}

	  		$grid.find('.grid-item:gt(3)').each(function(idx){
	  			if(idx%3 != 0) return true;
	  			if($(this).offset().top + ($(this).height()/2) > scroll_middle)
	  			{
	  			 $grid.masonry('remove', $grid.find('.grid-item').not(':lt('+(idx+3)+')') ).masonry('layout');
	  			}
	  		});

				count = $grid.find('.grid-item').length;
		  }else{
			  if(count > 6 && scroll >= boundary) {
				  remove_enable = true;
			  }
		  }

			scrollHeaderObserve();
	  });

		scrollHeaderObserve();

    $(".boram-category2").find("a").on("click",function(){
		var ojce = $(this).position();
		var indexnum = $(this).parents().index();
		var varsize = $(this).attr("data-barsize");
		 //$('.boram-category2 .tabBar').animate({left:185*$(this).parents().index()+"px"},"fast");
		 $('.boram-category2 .tabBar').animate({left:ojce.left+"px"},"fast");
		 //$('.boram-category2 .tabBar').animate({width:varsize+"px"},"fast");
		 $('.boram-category2 .tabBar').css("width",varsize+"px");
		 $('.cate-slider').slick('setPosition', 0);
    });
	var groupTotallogohoverarr = new Array();
	$(".boram-groupTotallogohover").find("div").on("mouseover",function(){
		var divindex = $(this).parents().index();
		var lineindex = Math.floor(divindex/6);
		groupTotallogohoverarr[divindex] = $(this).css("background-position");
		var jbSplit = groupTotallogohoverarr[divindex].split(' ');
		var positionY = -482;
		if(lineindex == 1){
			positionY = -487;
		}else if(lineindex == 2){
			positionY = -497;
		}else if(lineindex == 3){
			positionY = -497;
		}
		$(this).css("background-position", jbSplit[0]+" "+((lineindex * 90 * -1) + positionY)+"px");
    });
	$(".boram-groupTotallogohover").find("div").on("mouseleave",function(){
		$(this).css("background-position", groupTotallogohoverarr[$(this).parents().index()]);
    });


	  var swiper = new Swiper('.award-area.gtie9 .award-slider', {
	     slidesPerView: 3,
	     centeredSlides: true,
	     loop: true,
	     pagination: {
	       el: '.swiper-pagination',
	       clickable: true,
	     },
	     autoplay: {
	       delay: 2000,
	       disableOnInteraction: false,
	     },
	     navigation: {
	       nextEl: '.swiper-button-next',
	       prevEl: '.swiper-button-prev',
	     },
	    slidesPerView: 'auto',
	    centeredSlides: true,
	    loop: true,
	    //slidesPerGroup: 1,
	    paginationClickable: true,
	    //spaceBetween: 60
	  });


});

function scrollHeaderObserve(){
	var scTop = $(window).scrollTop();
	var tbHeight = $('.top-banner').is(":visible") ? $('.top-banner').height() : 0;

	$("#header, .menu-totalBg").css("top", Math.max(0, tbHeight - scTop) + "px");
}

$(window).resize(function(){
	scrollHeaderObserve();
});

function setCookieMobile ( name, value, expiredays ) {
    var todayDate = new Date();
    todayDate.setDate( todayDate.getDate() + expiredays );
    document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
}

function getCookieMobile ( name, value ) {
    var cookiedata = document.cookie;
    if ( cookiedata.indexOf(name + "=" + value) < 0 ){
		 //console.log("No cookie.");
         return false;
    }
    else {
	    //console.log("Cookie exists.");
        return true;
    }
}



$(".top-banner").each(function(){
	var t = this;
	if(!getCookieMobile("topBanner", 1) &&  $(".top-banner").find(".banner-item").length > 0)
	{
		$(t).show();
		$(".top-banner .banner-item img").css({'width':$(window).outerWidth(false) + 'px'});
		$(".top-banner-item-container").slick({
			slidesToShow: 1,
		  draggable: true,
		  swipeToSlide: true,
		  arrows: false,
		  dots: false,
		  fade: true,
		  cssEase: 'linear',
		  infinite: true
		});
		
		scrollHeaderObserve();
	}

	$(this).find(".btn-close-banner").click(function(){
		$(t).hide();
		if($(this).siblings().find("[type=checkbox]").prop("checked"))
		{
			setCookieMobile ( "topBanner", 1, 1 );
		}

		scrollHeaderObserve();
	});

});


$(".m-popup").draggable({containment: "body"});

$(".m-popup").each(function(){
	var t = this;
	if(!getCookieMobile("mPopup" + $(t).attr("data-id"), $(t).attr("data-id")))
	{
		$(t).show();
		window.scrollTo(0, 0);
	}

	$(this).find(".btn-close-pop").click(function(){
		$(t).hide();
		if($(t).find("[type=checkbox]").prop("checked"))
		{
			setCookieMobile ( "mPopup" + $(t).attr("data-id"), $(t).attr("data-id"), 1 );
		}
	});

});

$(window).load(function(){
	$(window).scrollTop(0);
});
