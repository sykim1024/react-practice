<?php if(count($bannerlist) > 5) { ?>
<style>
.main-slider .slick-dots li, .main-slider .slick-dots li .sd-con, .main-slider .slick-dots li a {width:<?php echo round(1300 / count($bannerlist), 2);?>px;}
</style>
<?php } ?>
<div class="top-banner">
	<div class="top-banner-item-container">
	<?php foreach($topbannerlist as $key => $value) { ?>
		
	<div class="banner-item" data-id="<?php echo $value['seed'];?>">
			<?php if (!empty($value['tb_link'])) {?>
				<a href="<?= $value['tb_link']?>" <?php if($value['tb_link_type'] == 1) { ?>target="_blank"<?php } ?> >
			<?php } ?>
			 <img src="<?php echo $value['tb_thumbnail'];?>">
			<?php if (!empty($value['tb_link'])) {?>
				</a>
			<?php } ?>




	</div>
	  <?php } ?>
	</div>
	<div class="btn-group">
		<div class="btn-container">
			<label class="btn-close-1"><input type="checkbox"><span></span>오늘 하루 보지 않기</label>
			<button class="btn-close-banner">X</button>
		</div>
	</div>
</div>

<?php foreach($popuplist as $popup) { ?>
<div class="m-popup" data-id="<?php echo $popup['seed'];?>" style="left:<?php echo $popup['pop_position_left'];?>px; top:<?php echo $popup['pop_position_top'];?>px;">
	<?php if($popup['pop_link']) { ?>
		<a href="<?php echo $popup['pop_link'];?>" target="<?php echo $popup['pop_link_type'] == 1 ? '_blank' : '_self';?>">
	<?php } ?>
  <img src="<?php echo $popup['pop_image'];?>">
		<?php if($popup['pop_link']) { ?>
			</a>
		<?php } ?>
  <div class="btn-group">
	<div class="btn-container">
		<label class="btn-close-1--"><input type="checkbox"><span></span>오늘 하루 보지 않기</label>
		<button class="btn-close-pop">X</button>
	</div>
  </div>
</div>
<?php } ?>
<div class="main-slider">
    <?php foreach($bannerlist as $key => $banner) { ?>
   <div class="main-slider-item<?php if($banner['bn_has_effect'] == 0) { ?> no-eff<?php } ?>" data-bg="<?php echo $banner['bn_image'];?>" data-title="<?php echo $banner['bn_name'];?>">
   	<div class="container">
   		<div class="slider-txt">
   			<h1 class="txt-key"># global</h1>
   			<h2><?php echo nl2br($banner['bn_text_major']);?></h2>
   			<div class="slider-txtSub">
          <?php
          $banner_text_minor_arr = explode("\r\n", $banner['bn_text_minor']);
          foreach($banner_text_minor_arr as $text) {
            echo '<p>'.$text.'</p>';
          }
          ?>
		  
		  <?php if($banner['bn_button_link']) { ?>
			<a href="<?php echo $banner['bn_button_link'];?>" class="btn-middle white" tabindex="0" style="
				left: 0;
				margin-top: 30px;
				margin-left: 0;
			" <?php if($banner['bn_button_link_type'] == 1) { ?>target="_blank"<?php } ?>><?php if($banner['bn_text_button']) { echo $banner['bn_text_button']; } else { echo '바로가기'; }?></a>
		  <?php } ?>
		  
   			</div>
   		</div>
   	</div><!--//container-->
   	<div class="slider-bg" style="background-image:url('<?php echo $banner['bn_image'];?>');"></div>
  </div><!--//slide<?php echo $key+1;?>-->
    <?php } ?>
 </div><!--//main-slider-->
 <div class="contents">
 	<h2 class="tle-con">business area</h2>
 	<h3 class="tle-sub">보람그룹의 고객사랑, 실천과 정성은 계속 됩니다</h3>
 	<ul class="tab-btn boram-category2">
		<li class="m8 on"><a href="#" data-barsize="155"><i class="ico-busi8"></i>보람그룹전체</a></li>
 		<li class="m1"><a href="#" data-barsize="155"><i class="ico-busi1"></i>상조사업</a></li>
 		<li class="m2"><a href="#" data-barsize="155"><i class="ico-busi2"></i>의전사업</a></li>
 		<li class="m3"><a href="#" data-barsize="155"><i class="ico-busi3"></i>장례식장사업</a></li>
 		<li class="m4"><a href="#" data-barsize="164"><i class="ico-busi4"></i>정보산업·무역사업</a></li>
 		<li class="m5"><a href="#" data-barsize="155"><i class="ico-busi5"></i>호텔·리조트사업</a></li>
 		<li class="m6"><a href="#" data-barsize="164"><i class="ico-busi6"></i>교육·스포츠사업</a></li>
 		<li class="m7"><a href="#" data-barsize="164"><i class="ico-busi7"></i>건설·신사업</a></li>
 		<li class="tabBar"></li>
 	</ul><!--tab-btn-->
 	<div class="tab-content">
		<div class="tabCon8 show">

	 		<div class="rt-area">
				<ul class="boram-groupTotal boram-groupTotallogohover">
					<li><div class="logo1">보람그룹</div></li>
					<li><div class="logo2">보람상조개발(주)</div></li>
					<li><div class="logo3">보람상조라이프(주)</div></li>
					<li><div class="logo4">보람상조애니콜(주)</div></li>
					<li><div class="logo5">보람상조피플(주)</div></li>
					<li><div class="logo6">보람카네기(주)</div></li>
					<li><div class="logo7">보람의정부장례식장</div></li>
					<li><div class="logo8">보람인천장례식장</div></li>
					<li><div class="logo9">보람창원장례식장</div></li>
					<li><div class="logo10">보람여수장례식장</div></li>
					<li><div class="logo11">보람김해장례식장</div></li>
					<li><div class="logo12">보람부산장례식장</div></li>
					<li><div class="logo13">보람정보산업(주)</div></li>
					<li><div class="logo14">IT color 보람종합현상소</div></li>
					<li><div class="logo15">LINCORY MOTORS(주)</div></li>
					<li><div class="logo16">캐슬비치호텔</div></li>
					<li><div class="logo17">프라임관광호텔</div></li>
					<li><div class="logo18">보람장례지도사교육원</div></li>
					<li><div class="logo19">보람할렐루야탁구단</div></li>
					<li><div class="logo20">보람장례문화연구소</div></li>
					<li><div class="logo21">비아로지스(주)</div></li>
					<li><div class="logo22">보람종합건설(주)</div></li>
					<li><div class="logo23">(주)신우종합건축사사무소</div></li>
				</ul>
	 		</div><!--//rt-area-->
 		</div><!--//tabCon8-->
 		<div class="tabCon1">
 			<div class="cate-left">
 				<h1>보람상조</h1>
				<h2>대한민국 대표상조 보람상조는 '내 부모 내 형제처럼 정성을 다하는' 기업이념 아래 상부상조의 본질적 가치를 실현합니다.</h2>
				<div class="cate-txt">
					국내 최초 링컨콘티넨탈 리무진 서비스를 제공하는 고품격 장례서비스</br>
					바다위의 특별한 경험을 선사하는 보람탐크루즈 여행서비스</br>
					특별한 시작을 원하는 분들께 감동과 가장 아름다운 추억을 드리는</br>
					웨딩 서비스 등 생활 전반에 걸친 토탈 라이프 케어 서비스를 제공합니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub1#tab1" class="btn-middle square whiteLine" onclick="">장례서비스</a>
					<a href="/Affiliated/Sub1#tab2" class="btn-middle square whiteLine" onclick="">여행서비스</a>
					<a href="/Affiliated/Sub1#tab3" class="btn-middle square whiteLine" onclick="">웨딩서비스</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate1-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate1-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate1-3.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon1-->
 		<div class="tabCon2">
 			<div class="cate-left">
 				<h1>의전사업</h1>
				<h2>그룹의 기업 경쟁력 강화, 고품격 명품 장례 서비스를<br>
위한 체계적인 장례 행사 전문 조직을 운영합니다.  </h2>
				<div class="cate-txt">
					보람상조는 오랜 장례행사 경험을 바탕으로 체계적인 교육을 통해<br>
보다 퀄리티 높은 서비스를 제공하며, 개인뿐만 아니라 기업 및 단체<br>
서비스 제휴 체결을 통해 제휴를 맺은 단체의 임직원들도 보람상조의<br>
특화된 장례서비스 혜택을 받으실 수 있습니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub2" class="btn-middle square whiteLine" onclick="">보람카네기</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate2-1.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon2-->
 		<div class="tabCon3">
 			<div class="cate-left">
 				<h1>장례식장사업</h1>
				<h2>전국에 있는 보람그룹의 직영 장례식장은 고인과 가족뿐만 아니라 장례식장을 찾아주신 모든 분들께 최고의 서비스를 제공합니다.</h2>
				<div class="cate-txt">
					품격있고 아름다운 장례문화를 창조하기 위해 보람그룹의 직영 장례식장은</br>
				항상 고객과 소통하며 발전하고 보람이라는 이름에서 줄 수 있는 정직과</br>신뢰를 바탕으로 고객에게 따뜻한 감동을 선사합니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom" style="margin-top:20px">
					<a href="/Affiliated/Sub3#tab1" class="btn-middle square whiteLine" onclick="">의정부 장례식장</a>
					<a href="/Affiliated/Sub3#tab2" class="btn-middle square whiteLine" onclick="">인천 장례식장</a>
					<a href="/Affiliated/Sub3#tab3" class="btn-middle square whiteLine" onclick="">창원장례식장</a>
					<a href="/Affiliated/Sub3#tab4" class="btn-middle square whiteLine" onclick="">김해 장례식장</a>
					<a href="/Affiliated/Sub3#tab5" class="btn-middle square whiteLine" onclick="">여수 장례식장</a>
					<a href="/Affiliated/Sub3#tab5" class="btn-middle square whiteLine" onclick="">부산장례식장</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate3-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate3-3.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate3-4.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate3-5.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate3-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/none-img.png"></div>
  			</div>
  			<!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon3-->
 		<div class="tabCon4">
 			<div class="cate-left">
 				<h1>정보산업·무역사업</h1>
				<h2>IT기술과 접목한 스마트한 기술 서비스로 고객들의 니즈에 <br>
신속하게 대응하고 고객만족도 향상을 위한 다양한 플랫폼을 제공합니다.</h2>
				<div class="cate-txt">
					항상 고객을 생각하는 마음으로 IT기술을 통한 보람만의 창의적인 생각과<br>
사람과 기술이 공존할 수 있는 따뜻한 세상을 위하여 끈임없이 발전하고<br>
노력하여 고객의 풍요로운 내일을 위한 미래를 개척합니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub4#tab1" class="btn-middle square whiteLine" onclick="">보람정보산업</a>
					<a href="/Affiliated/Sub4#tab2" class="btn-middle square whiteLine" onclick="">보람종합현상소</a>
					<a href="/Affiliated/Sub4#tab3" class="btn-middle square whiteLine" onclick="">LINCORY MOTORS</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate4-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate4-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate4-3.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon4-->
 		<div class="tabCon5">
 			<div class="cate-left">
 				<h1>호텔·리조트사업</h1>
				<h2>편안함과 최고의 휴식을 위한 고품격 서비스를 제공하고<br>
모두가 함께 할 수 있는 여가 문화를 위해 앞장섭니다.</h2>
				<div class="cate-txt">
					세련된 객실과 다양한 부대시설을 갖춘 호텔, 리조트 서비스를<br>
제공하며, 가족 모두가 함께할 수 있는 따뜻하고 멋진 전경을 선사하고<br>
최고의 힐링과 아름다운 추억을 선물합니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub5#tab1" class="btn-middle square whiteLine" onclick="">캐슬비치호텔</a>
					<a href="/Affiliated/Sub5#tab2" class="btn-middle square whiteLine" onclick="">프라임관광호텔</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate5-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate5-2.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon5-->
 		<div class="tabCon6">
 			<div class="cate-left">
 				<h1>교육·스포츠사업</h1>
				<h2>대한민국의 장례문화 교육과 스포츠 발전을 위하여<br>
보람그룹은 항상 노력하고 최선을 다하겠습니다.</h2>
				<div class="cate-txt">
					오랜 기간 축적된 경험을 바탕으로 장례 이론과 실무 행사 진행 능력까지 <br>
갖춘 전문 장례지도사를 양성하고 국민 건강 증진을 위한 생활 <br>
체육대회 주최 및 중, 고 탁구연맹 후원, 남자실업 탁구단인 '보람할렐루야'를 <br>
창단하여 운영하고 있습니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub6#tab1" class="btn-middle square whiteLine" onclick="">보람장례지도사교육원</a>
					<a href="/Affiliated/Sub6#tab2" class="btn-middle square whiteLine" onclick="">보람할렐루야탁구단</a>
					<a href="/Affiliated/Sub6#tab3" class="btn-middle square whiteLine" onclick="">한국상장례문화연구소</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate6-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate6-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate6-3.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon6-->
 		<div class="tabCon7">
 			<div class="cate-left">
 				<h1>건설·신사업</h1>
				<h2>보람그룹은 끊임없는 도전과 열정으로 미래를 설계하고<br>
혁신을 주도하여 발전하고 성장하는 기업이 되겠습니다.</h2>
				<div class="cate-txt">
					최첨단 기술집약형 엔지니어링 사업, 공사 관리, 세계 최초 재활용이 <br>
불가능한 낭연 스티로폼을 재활용한 고부가 가치를 실현하여 사람과 <br>
자연을 생각하는 친환경 에코 사업 등 미래 발전을 위한 다양한<br>
사업을 구상하고 실현합니다.
				</div><!--//cate-txt-->
				<div class="btn-bottom">
					<a href="/Affiliated/Sub7#tab1" class="btn-middle square whiteLine" onclick="">비아로지스</a>
					<a href="/Affiliated/Sub7#tab2" class="btn-middle square whiteLine" onclick="">보람종합건설</a>
					<a href="/Affiliated/Sub7#tab4" class="btn-middle square whiteLine" onclick="">신우종합건축사사무소</a>
				</div><!--//btn-bottom-->
 			</div><!--//cate-left-->
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate7-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate7-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate7-4.png"></div>
  			</div><!--//cate-slider-->
 			</div><!--//cate-right-->
 		</div><!--//tabCon7-->
 		<!--
		<div class="tabCon8">
 			<div class="cate-left">
 				<h1>보람상조</h1>
				<h2>대한민국 대표상조 보람상조는 '내 부모 내 형제처럼 정성을 다하는' 기업이념 아래 상부상조의 본질적 가치를 실현합니다.</h2>
				<div class="cate-txt">
					국내 최초 링컨콘티넨탈 리무진 서비스를 제공하는 고품격 장례서비스</br>
					바다위의 특별한 경험을 선사하는 보람탐크루즈 여행서비스</br>
					특별한 시작을 원하는 분들께 감동과 가장 아름다운 추억을 드리는</br>
					웨딩 서비스 등 생활 전반에 걸친 토탈 라이프 케어 서비스를 제공합니다.
				</div>
				<div class="btn-bottom">
					<a href="#" class="btn-middle square whiteLine" onclick="return false;">장례서비스</a>
					<a href="#" class="btn-middle square whiteLine" onclick="return false;">여행서비스</a>
					<a href="#" class="btn-middle square whiteLine" onclick="return false;">웨딩서비스</a>
				</div>
 			</div>
 			<div class="cate-right">
 				<div class="cate-slider basic-dots">
  				 <div><img src="/images/client/pc/kor/img/img-cate1-1.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate1-2.png"></div>
  				 <div><img src="/images/client/pc/kor/img/img-cate1-3.png"></div>
  			</div>
 			</div>
 		</div>
		-->
 	</div><!--//tab-con-->
 </div><!--//contents-->


 <div class="magazine-area">
 	 <div class="magazine-slider">
     <?php foreach($magazinelist1 as $key => $magazine1) { ?>
       <div class="mz<?php echo $key+1;?>" style="<?php if($magazine1['mg_blur_thumbnail']) { ?>background-image:url('<?php echo $magazine1['mg_blur_thumbnail'];?>'); background-position:center 25%; background-size:1920px auto;<?php } ?>">
  	   	<div class="contents">
  	   		<div class="img-con">
  		   		<img src="<?php echo $magazine1['mg_thumbnail'];?>" onerror="/images/client/pc/kor/img/img-megazineCon1.jpg">
  		   	</div>
  		   	<div class="mz-con">
  		   		<h2 class="tle-con">boram magazine</h2>
  		   		<div class="mz-conSub">
  		   			<span class="cate">[<?php echo $magazine1['mc_name'];?>]</span>
  		   			<h3><?php echo nl2br($magazine1['mg_title']); ?></h3>
  		   			<div class="con-txt">
  		   				<?php echo nl2br(mb_substr($magazine1['mg_contents'], 0, 200, "UTF-8")); ?>
  		   			</div>
  		   			<a href="<?php echo $magazine1['mg_link'] ? $magazine1['mg_link'] : '/Promotion/Sub1/dview/'.$magazine1['seed'];?>" class="btn-more" <?php if($magazine1['mg_link_type'] == 0) { ?>target="_self"<?php } else { ?>target="blank"<?php } ?>>more</a>
  		   		</div><!--//mz-conSub-->
  		   		<div class="mzBtn-group">
  						<button type="button" class="mz-prev">이전</button>
  				 		<button type="button" class="mz-next">다음</button>
  					</div><!--//mzBtn-group-->
  					<div class="slide-count-wrap">
  		        <span class="current"></span><div class="slash"></div><span class="total"></span>
  		      </div><!--//slide-count-wrap-->
  		   	</div><!--//mz-con-->
  	   	</div><!--//contents-->
  	   </div><!--//mz<?php echo $key+1;?>-->
     <?php } ?>
	 </div><!--//magazine-slider-->
 </div><!--//magazine-area-->
 <div class="contents">
 	<ul class="magazine-list">
	<?php $prev_od = 4; ?>
    <?php foreach($magazinelist2 as $key => $magazine2) { ?>
		<?php if(abs($magazine2['mg_od_no'] - $prev_od) > 1) { 
		  $margin_left = (abs($magazine2['mg_od_no'] - $prev_od) - 1) * 455;
		} else { 
		  $margin_left = $magazine2['mg_od_no'] == 3 ? 0 : 65;
		}
		
		$prev_od = $magazine2['mg_od_no'];
		?>
		
 		<li style="margin-left:<?php echo $margin_left;?>px;">
 			<div class="mz-box">
 				<div class="mz-img"><img src="<?php echo $magazine2['mg_thumbnail'];?>" onerror="/images/client/pc/kor/img/mz-boxCon1.jpg"></div>
 				<div class="mz-boxTxt">
 					<span class="mz-boxCate"><?php echo $magazine2['mc_name_sub'] ? $magazine2['mc_name_sub'] : $magazine2['mc_name'];?></span>
 					<a href="<?php echo $magazine2['mg_link'] ? $magazine2['mg_link'] : '/Promotion/Sub1/dview/'.$magazine2['seed'];?>" <?php if($magazine2['mg_link_type'] == 0) { ?>target="_self"<?php } else { ?>target="blank"<?php } ?>><?php echo nl2br($magazine2['mg_title']);?></a>
 					<span class="upload-date"><?php echo $magazine2['mg_virtual_date'] ? $magazine2['mg_virtual_date'] : date("Y-m-d", strtotime($magazine2['mg_insert_datetime']));?></span>
 				</div><!--//mz-boxTxt-->
 			</div><!--//-mz-box-->
 		</li>
    <?php } ?>
 	</ul>
 </div><!--//contents-->

 <div class="contents">
 	<h2 class="tle-con">social live</h2>
 	<h3 class="tle-sub">보람의 새로운 소식을 전해드립니다.</h3>
 	<ul class="btn-social">
 		<li class="sns1 on"><a href="#" class="btn-all" data-type=""><i></i>ALL</a></li>
 		<li class="sns2"><a href="#" class="btn-facebook" data-type="facebook"><i></i>페이스북</a></li>
 		<li class="sns3"><a href="#" class="btn-instagram" data-type="insta"><i></i>인스타그램</a></li>
 		<li class="sns4"><a href="#" class="btn-youtube" data-type="youtube"><i></i>유투브</a></li>
 	</ul><!--tab-btn-->
 	<div class="social-con">
 		<div>
 			<ul>

			</ul>
			<div class="btn-more">
				<a href="/">더보기</a>
			</div><!--//btn-more-->
 		</div>
 	</div><!--//social-con-->
 </div><!--//contents-->


 <!--ie9제외 나머지 브라우저-->
 <div class="award-area gtie9">
 	<h2 class="tle-con">award</h2>
 	<h3 class="tle-sub">보람그룹의 노력과 땀의 결실인 명예로운 수상 소식입니다.</h3>
 	<div class="award-slider swiper-container">
    <div class="swiper-wrapper">
      <?php if(count($awardlist) > 1) {
        $award_first = array_splice($awardlist, 0, 1);

        $awardlist[] = $award_first[0];
       } ?>
      <?php foreach($awardlist as $key => $award) { ?>
      <div class="award-item swiper-slide">
        <a href="<?php echo $award['awd_link'];?>" <?php if($award['awd_link_type'] == 0) { ?>target="_self"<?php } else { ?>target="blank"<?php } ?>>
        	<div class="award-logo"><img src="<?php echo $award['awd_thumbnail'];?>" onerror="/images/client/pc/kor/img/ico-awardLogo1.png"></div>
        	<div class="award-info">
        		<strong><?php echo $award['awd_inst'];?></strong>
            <?php
            $awd_title_arr = explode("\r\n", $award['awd_title']);
            foreach($awd_title_arr as $awd_title_line) {
              echo "<p>".$awd_title_line."</p>";
            }
            ?>
        	</div>
        </a>
      </div><!--//award-item-->
      <?php } ?>
    </div><!--//swiper-wrapper-->
    <div class="swiper-pagination"></div>
  </div><!--//award-slider.swiper-container-->
  </div><!--//award-slider-->
 </div><!--//award-area-->


<div class="award-area ie9">
 	<h2 class="tle-con">award</h2>
 	<h3 class="tle-sub">보람그룹의 노력과 땀의 결실인 명예로운 수상 소식입니다.</h3>
 		<div class="award-slider">
      <?php foreach($awardlist as $key => $award) { ?>
	    <div>
	      <div class="award-item">
          <a href="<?php echo $award['awd_link'];?>" <?php if($award['awd_link_type'] == 0) { ?>target="_self"<?php } else { ?>target="blank"<?php } ?>>
            <div class="award-logo"><img src="<?php echo $award['awd_thumbnail'];?>" onerror="/images/client/pc/kor/img/ico-awardLogo1.png"></div>
          	<div class="award-info">
          		<strong><?php echo $award['awd_inst'];?></strong>
              <?php
              $awd_title_arr = explode("\r\n", $award['awd_title']);
              foreach($awd_title_arr as $awd_title_line) {
                echo "<p>".$awd_title_line."</p>";
              }
              ?>
          	</div>
          </a>
	      </div><!--//award-item-->
	    </div>
      <?php } ?>
	  </div><!--//award-slider-->
 </div><!--//award-area-->

 <div class="recruit-area">
  <?php foreach($bottombannerlist as $key => $bottombanner) { if($key > 1) break;?>
    <div class="recruit-box rc-section<?php echo $key + 1;?>">
   		<strong><?php echo $bottombanner['bb_text_major'];?></strong>
   		<p><?php echo $bottombanner['bb_text_minor'];?></p>
   		<a href="<?php echo $bottombanner['bb_button_link'];?>" <?php if($bottombanner['bb_button_link_type'] == 1) { ?>target="_blank"<?php } ?>><?php echo $bottombanner['bb_text_button'];?></a>
   		<div class="bg" style="background-image:url('<?php echo $bottombanner['bb_thumbnail'];?>');"></div>
   	</div>
  <?php } ?>
 </div><!--//recruit-area-->
 <div class="affil-area">
 	<h2 class="tle-con">보람그룹 계열사</h2>
 	<h3 class="tle-sub">보람과 역사를 함께한 계열사를 소개합니다</h3>
 	<ul class="affilTotal-list">
 		<li>
 			<strong>상조사업</strong>
 			<ul class="affil-depth">
 				<li>보람상조개발(주)</li>
 				<li>보람상조라이프(주)</li>
 				<li>보람상조애니콜(주)</li>
 				<li>보람상조피플(주)</li>
 			</ul>
 		</li>
 		<li>
 			<strong>의전사업</strong>
 			<ul class="affil-depth">
 				<li>보람카네기(주)</li>
 			</ul>
 		</li>
 		<li>
 			<strong>장례식장사업</strong>
 			<ul class="affil-depth">
 				<li>보람의정부장례식장</li>
 				<li>보람인천장례식장</li>
 				<li>보람창원장례식장</li>
 				<li>보람김해장례식장</li>
 				<li>보람여수장례식장</li>
 				<li>보람부산장례식장</li>
 			</ul>
 		</li>
 		<li>
 			<strong>정보산업·무역사업</strong>
 			<ul class="affil-depth">
 				<li>보람정보산업(주)</li>
 				<li>보람종합현상소</li>
 				<li>LINCORY MOTORS(주)</li>
 			</ul>
 		</li>
 		<li>
 			<strong>호텔·리조트사업</strong>
 			<ul class="affil-depth">
 				<li>캐슬비치호텔</li>
 				<li>프라임관광호텔</li>
 			</ul>
 		</li>
 		<li>
 			<strong>교육·스포츠사업</strong>
 			<ul class="affil-depth">
 				<li>보람장례지도사교육원</li>
 				<li>보람할렐루야탁구단</li>
 				<li>한국상장례문화연구소</li>
 			</ul>
 		</li>
 		<li>
 			<strong>건설·신사업</strong>
 			<ul class="affil-depth">
 				<li>비아로지스(주)</li>
 				<li>보람종합건설(주)</li>
 				<li>(주)신우종합건축사사무소</li>
 			</ul>
 		</li>
 	</ul>
 </div><!--//affiliated-area-->
<script>
var sl_type = 0;
var showAll = false;
var getcnt = 3;
</script>
